# ToS FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

This module allowing you to attach your Terms of sale to PDF documents (invoices, propals, orders and shippings)

This is fork of orginal module written by : [Mikael Carlavan](http://www.mikael-carlavan.fr)

## Features

* Register sevral ToS (in PDF fromat) with module admin page.
* Define ToS that attached by default on each documents.
* You can chose ToS that attached on document when you create it.
* You can update ToS that attached on document after it's created.
* ToS can be configued to applied on each pages of generated document.

Other modules are available on [Dolistore.com](https://www.dolistore.com>).

## **Disclaimer**

Several users report got an empty white "Modules/Application setup" page after they activated the module (see [this thread on the french forum](https://www.dolibarr.fr/forum/t/modules-applications-page-vide-apres-installation-du-module-des-conditions-generales-de-vente-tos-3-0-0/34295)). Unfortunately i did not be able to reproduce this error and i couldn't provide a solution.

__If you got this problem, you must remove the `tos` directory from the custom module directory, this will be sufficient to resolve the problem.

## Installation

### From the ZIP file and GUI interface

- If you get the module in a zip file (like when downloading it from the market place [Dolistore](https://www.dolistore.com)), go into
menu ```Home - Setup - Modules - Deploy external module``` and upload the zip file.

### From a GIT repository

- Clone the repository in ```$dolibarr_main_document_root_alt/tos```

```sh
cd ....../custom
git clone https://framagit.org/indelog/dolibarr-tos
```

### <a name="final_steps"></a>Final steps

From your browser:

  - Log into Dolibarr as a super-administrator
  - Go to "Setup" -> "Modules"
  - You should now be able to find and enable the module

## Use

  - Go to module configiration page for ToS
    - Upload your ToS by using the didicated form.
    - Chose the ToS that must be applied by default on each document creation *(the choose "No terms of sale" means no ToS are applied)*.
    - If you need your ToS be applied on each page of generated documument check the *"Insert automatically ToS to generated documents"* button.
  - When creating new order, invoice, shipping or propal, you can chose the ToS that attached with it by using the *"Terms of sale"* field.
    - You can modify ToS atteched with created document by editing the *"Terms of sale"* field.

## Licenses

### Main code

GPLv3. See file COPYING for more information.

### Documentation

All texts and readmes are licensed under GFDL.
