# ToS POUR [DOLIBARR ERP CRM](https://www.dolibarr.org)

Ce module vous donne la possibilite d'ajouter vos Conditions générales de vente aux documents PDF (factures, propositions, commandes et expéditions).

Ceci est un fork du module originellement écrit par : [Mikael Carlavan](http://www.mikael-carlavan.fr)

## Features

  * Enregistez plusieurs CGV (au format PDF) via la page d'administration du module.
  * Définissez les conditions d'utilisation attachés par défaut aux documents.
  * Vous pouvez quel CGV seront attachées aux documents lors de sa création. 
  * Vous pouvez modifier les CGV attachées aux documents après leurs création.
  * Le module peut être configurer pour insérer les CGV sur toutes les pages des documents générés.

D'autres modules sont disponibles sur le [Dolistore](https://www.dolistore.com>).

## **Avertissement**

Plusieurs utilisateurs ont reporté qu'après l'activation du module la page *Configuration Modules/Application* devenait vierge (elle n'affiche plus rien). Voire le fils de discussion sur le [Forum](https://www.dolibarr.fr/forum/t/modules-applications-page-vide-apres-installation-du-module-des-conditions-generales-de-vente-tos-3-0-0/34295/8). Je n'ai malheureusement pas réussi à reproduire le problème et donc à l'identifier pour lui apporter une solution.

__Si vous le rencontrer, il vous suffît de supprimer le dossier `tos` du dossier d'installation des modules pour retrouver l'accès à la page *Configuration Modules/Application*.__

## Installation

### Depuis le fichier ZIP et l'interface graphique

- Si vous obtenez ce module via un fichier ZIP (ce qui est le cas si vous le récupérer depuis le [Dolistore](https://www.dolistore.com)), rendez-vous le menu ```Accueil - Configuration - Modules/Applications - Déployer/Installer un module externe``` et téléverser le fichier .zip.

### Depuis le dépôt GIT

- Cloner le dépot dans ```$dolibarr_main_document_root_alt/tos```

```sh
cd ....../custom
git clone https://framagit.org/indelog/dolibarr-tos
```

### <a name="final_steps"></a>Étape final

Depuis votre navigateur :

  - Connectez-vous à votre instance Dolibarr avec un compte super administrateur
  - Rendez-vous dans "Configuration" -> "Modules/Applications"
  - Vous devriver voire et être en meusure d'activer le module.

## Utilisation

  - Se rendre sur la page de confuguration du module CGV.
    - Upload your ToS by using the didicated form.
    - Ajoutez vous conditions d'utilisation via le forumlaire dédié.
    - Choisissez les CGV qui doivent s'appliquer par default *(le choix "Aucune condition de vente" signifie qu'aucunes condition de vente ne seront attachées)*.
    - Si vous avez besoin que vos CGV figure sur chaque pages des documents générés, activer l'option *"Ajouter les CGV sur chaque page"*.
  - Quand vous créez un nouveau document commende, facture, proposition ou expédition, vous pouvez choisir les CGV attaché via le champ *"Conditions générales de vente"*.
    - Vous pouvez modifer les CGV attachées à un document en éditant le champ *"Conditions générales de vente"*.

## Licenses

### Main code

GPLv3. See file COPYING for more information.

### Documentation

All texts and readmes are licensed under GFDL.
