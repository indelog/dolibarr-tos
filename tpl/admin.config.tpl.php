<?php

/* Copyright (C) 2012      Mikael Carlavan        <contact@mika-carl.fr>
 *                                                http://www.mikael-carlavan.fr
 * Copyright (C) 2020      Maxime DEMAREST        <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**	    \file       htdocs/tos/tpl/admin.config.tpl.php
 *		\ingroup    tos
 *		\brief      Admin setup view
 */

llxHeader('', $langs->trans("CGVSetup"), '', '', 0, 0);

echo ($message ? dol_htmloutput_mesg($message, '', ($error ? 'error' : 'ok'), 0) : '');

echo print_fiche_titre($langs->trans("CGVSetup"), $linkback, 'setup');

dol_fiche_head($head, 'config', $langs->trans("CGV"), -1);

// List of uploaded cgv
//echo $formfile->showdocuments('tos', '', $conf->tos->dir_output, '', 0, 1, '', 0, 0, 0, 0, 0, '', $langs->trans("ReadCGVFile"));
$formfile->list_of_documents(
	$filearray = $files,
	$object = '',
	$modulepart = 'tos',
	$param = '',
	$forcedownload = 0,
	$relativepath = '',
	$permonobjets = 1,
	$useinecm = 0,
	$textifempty = $langs->trans('NoTosUploaded'),
	$maxlength = 0,
	$title = $langs->trans("ReadCGVFile"),
);

// Form to upload new cgv
$formfile->form_attach_new_file($_SERVER['PHP_SELF'].'?action=sendit', $langs->trans("SelectCGVFile"), 1, 0, 1, 50, '', '', 1, '', 0, '', '.pdf');

?>

<span class="opacitymedium"><?php echo $langs->trans("ToSOptions"); ?></span><br><br>

<table class="noborder centpercent">
	<tr class="liste_titre">
        <td class="width50p"><?php echo $langs->trans("Name"); ?></td>
        <td class="center"><?php echo $langs->trans("Value"); ?></td>
    </tr>
    <tr class="oddeven">
        <td><?php echo $langs->trans("DefaultCGV"); ?></td>
		<td class="center">
			<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
			<input type="hidden" name="token" value="<?php echo newToken(); ?>" />
			<input type="hidden" name="action" value="updatedefaulttos" />
			<table class="nobordernopadding centpercent"><tr>
				<td><?php echo $html->selectarray("tos", $files_for_select, $conf->global->TOS_DEFAULT_FILE); ?></td>
				<td><input type="submit" class="button" value="<?php echo $langs->trans("Modify"); ?>" ></td>
			</tr></table>
		</td>
    </tr>
    <tr class="oddeven">
        <td><?php echo $langs->trans("AddTosOnEachPage"); ?></td>
        <td class="center"><?php echo $linkAddToSPage; ?></td>
    </tr>
</table>
</form>

<?php
// Confirm before delete CGV
if ($action == 'delete') {
	print $form->formconfirm(
			$_SERVER["PHP_SELF"].'?urlfile='.urlencode($urlfile),
			$langs->trans('DeleteFile'),
			$langs->trans('ConfirmDeleteFile'),
			'confirm_deletefile',
			'',
			0,
			1
	);
}
?>

<br />
<?php llxFooter(''); ?>
