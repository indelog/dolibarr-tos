<?php
/* Copyright (C) 2012      Mikael Carlavan        <contact@mika-carl.fr>
 *                                                http://www.mikael-carlavan.fr
 * Copyright (C) 2020-2021      Maxime Demarest        <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * 		\defgroup   modToS     Module ToS
 *      \file       htdocs/core/modules/modToS.class.php
 *      \ingroup    modToS
 *      \brief      Description and activation file for module modToS
 */
include_once(DOL_DOCUMENT_ROOT ."/core/modules/DolibarrModules.class.php");


/**
 * 		\class      modToS
 *      \brief      Description and activation class for module modToS
 */
class modToS extends DolibarrModules
{
	/**
	 *   \brief      Constructor. Define names, constants, directories, boxes, permissions
	 *   \param      DB      Database handler
	 */
	function __construct($db)
	{
        global $langs, $conf;

        $this->db = $db;
		// Id for module (must be unique).
		// Use here a free id (See in Home -> System information -> Dolibarr for list of used modules id).
		$this->numero = 140300;
		// Key text used to identify module (for permissions, menus, etc...)
		$this->rights_class = 'tos';

		// Family can be 'crm','financial','hr','projects','products','ecm','technic','other'
		// It is used to group modules in module setup page
		$this->family = "technic";
		// Module label (no space allowed), used if translation string 'ModuleXXXName' not found (where XXX is value of numeric property 'numero' of module)
		$this->name = 'tos';
		// Module description, used if translation string 'ModuleXXXDesc' not found (where XXX is value of numeric property 'numero' of module)
		$this->description = "Ajout de CGV aux documents PDF";
        $this->descriptionlong = "Module permettant d'attacher vos CGV aux documents PDF (factures, devis, commandes).";
        $this->editor_name = 'Indelog';
        //$this->editor_url = 'https://www.indelog.fr';

		// Possible values for version are: 'development', 'experimental', 'dolibarr' or version
		$this->version = '3.0.1';
		// Key used in llx_const table to save module status enabled/disabled (where MYMODULE is value of property name of module in uppercase)
		$this->const_name = 'MAIN_MODULE_'.strtoupper($this->name);

        // Module position in the family on 2 digits ('01', '10', '20', ...)
        $this->module_position = '90';

		// Name of image file used for this module.
		// If file is in theme/yourtheme/img directory under name object_pictovalue.png, use this->picto='pictovalue'
		// If file is in module/img directory under name object_pictovalue.png, use this->picto='pictovalue@module'
		$this->picto = 'tos@tos';

		// Defined all module parts (triggers, login, substitutions, menus, css, etc...)
		// for default path (eg: /mymodule/core/xxxxx) (0=disable, 1=enable)
		// for specific path of parts (eg: /mymodule/core/modules/barcode)
		// for specific css file (eg: /mymodule/css/mymodule.css.php)
		//$this->module_parts = array(
		//                        	'triggers' => 0,                                 	// Set this to 1 if module has its own trigger directory (core/triggers)
		//							'login' => 0,                                    	// Set this to 1 if module has its own login method directory (core/login)
		//							'substitutions' => 0,                            	// Set this to 1 if module has its own substitution function file (core/substitutions)
		//							'menus' => 0,                                    	// Set this to 1 if module has its own menus handler directory (core/menus)
		//							'theme' => 0,                                    	// Set this to 1 if module has its own theme directory (core/theme)
		//                        	'tpl' => 0,                                      	// Set this to 1 if module overwrite template dir (core/tpl)
		//							'barcode' => 0,                                  	// Set this to 1 if module has its own barcode directory (core/modules/barcode)
		//							'models' => 0,                                   	// Set this to 1 if module has its own models directory (core/modules/xxx)
		//							'css' => array('/mymodule/css/mymodule.css.php'),	// Set this to relative path of css file if module has its own css file
	 	//							'js' => array('/mymodule/js/mymodule.js'),          // Set this to relative path of js file if module must load a js on all pages
		//							'hooks' => array('hookcontext1','hookcontext2')  	// Set here all hooks context managed by module
		//							'workflow' => array('WORKFLOW_MODULE1_YOURACTIONTYPE_MODULE2'=>array('enabled'=>'! empty($conf->module1->enabled) && ! empty($conf->module2->enabled)', 'picto'=>'yourpicto@mymodule')) // Set here all workflow context managed by module
		//                        );
		$this->module_parts = array(
			'triggers' => 1,
			'hooks' => array('pdfgeneration'),
		);

		// Data directories to create when module is enabled.
		// Example: this->dirs = array("/mymodule/temp");
		$this->dirs = array("/tos/temp");
		$r=0;

		// Config pages. Put here list of php page names stored in admmin directory used to setup module.
		$this->config_page_url = array('config.php@tos');

		// Dependencies
		$this->depends = array();		// List of modules id that must be enabled if this module is enabled
		$this->conflictwith = array();
        $this->requiredby = array();
		$this->phpmin = array(7,0);					// Minimum version of PHP required by module
		$this->need_dolibarr_version = array(11,0);	// Minimum version of Dolibarr required by module
		$this->langfiles = array("tos@tos");

		// Constants
		$this->const = array();

        $this->tabs = array();

        // Dictionnaries
        $this->dictionnaries = array();

        // Boxes
		// Add here list of php file(s) stored in includes/boxes that contains class to show a box.
        $this->boxes = array();			// List of boxes


		// Permissions
		$this->rights = array();		// Permission array used by this module
		$r = 0;

		// Main menu entries
		$this->menu = array();			// List of menus to add

        $r = 0;

	}

	/**
	 *		Function called when module is enabled.
	 *		The init function add constants, boxes, permissions and menus (defined in constructor) into Dolibarr database.
	 *		It also creates data directories.
	 *      @return     int             1 if OK, 0 if KO
	 */
	function init($options = '')
	{
	   global $conf;

		$sql = array();

        // Create extrafields during init
        include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
        $extrafields = new ExtraFields($this->db);

		$tosSelectParam = array('NoCgv'=>'NoCgv');
		// Get list of tos allready uploaded
		include_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
		foreach (dol_dir_list(DOL_DATA_ROOT.'/'.$this->name, 'files', 0, '\.pdf$', '\.meta$', '', SORT_ASC,1) as $f)
			$tosSelectParam[$f['name']] = preg_replace('/\.[a-z]{3}$/', '', $f['name']);

		// invoice
		foreach(array('commande', 'expedition', 'facture', 'propal') as $elem)
			$extrafields->addExtraField(
				$attrname = 'tos_attached',
				$label = 'CGV',
				$type = 'select',
				$pos = 10,
				$size = '',
				$elementtype = $elem,
				$unique = 0,
				$required = 1,
				$default_value = (empty($conf->global->TOS_DEFAULT_FILE) ? 'NoCgv' : $conf->global->TOS_DEFAULT_FILE),
				$param = array('options'=>$tosSelectParam),
				$alwayseditable = 0,
				$perms = '',
				$list = -1,
				$help = '',
				$computed = '',
				$entity = '',
				$langfile = 'tos@tos',
				$enabled = '$conf->tos->enabled',
			);

		return $this->_init($sql, $options);
	}

	/**
	 *		Function called when module is disabled.
	 *      Remove from database constants, boxes and permissions from Dolibarr database.
	 *		Data directories are not deleted.
	 *      @return     int             1 if OK, 0 if KO
	 */
	function remove($options = '')
	{
		$sql = array();

		return $this->_remove($sql, $options);
	}
}
?>
