export VERSION = `git tag | tail -n1`
export ZIP_FILE = bin/module_tos-${VERSION}.zip

all: clean makepackconf zip

zip:
	echo " * Create zip file"
	if ! test -d tos/ ; then mkdir tos/ ; fi
	sed -e "/^$$\|#\|[ ]/d" build/makepack-tos.conf | xargs cp --parents -t tos/
	if test -f $(ZIP_FILE) ; then rm $(ZIP_FILE) ; fi
	zip -r $(ZIP_FILE) tos/

makepackconf:
	echo " * Create makepack config"
	sed -e "s/__VERSION__/$(VERSION)/" -e "s/__YEAR__/`date +%Y`/" build/makepack-tos.head  > build/makepack-tos.conf
	git ls-files --no-empty-directory | sed -e "/^Makefile$$/d" -e "/^\.gitignore$$/d" -e "/^build\/makepack-tos\.head$$/d" -e "/^\.editorconfig$$/d"  >> build/makepack-tos.conf

clean:
	echo " * Clean"
	if test -d tos/ ; then rm -r tos ; fi
