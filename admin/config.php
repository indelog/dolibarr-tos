<?php
/* Copyright (C) 2014      Mikael Carlavan        <contact@mika-carl.fr>
 *                                                http://www.mikael-carlavan.fr
 * Copyright (C) 2020      Maxime Demarest        <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


/**
 *      \file       htdocs/tos/admin/config.php
 *		\ingroup    tos
 *		\brief      Page to setup tos module
 */

$res = 0;
// Try main.inc.php into web root known defined into CONTEXT_DOCUMENT_ROOT (not always defined)
if ($res && !empty($_SERVER["CONTEXT_DOCUMENT_ROOT"])) $res = @include $_SERVER["CONTEXT_DOCUMENT_ROOT"]."/main.inc.php";
// Try main.inc.php into web root detected using web root calculated from SCRIPT_FILENAME
$tmp = empty($_SERVER['SCRIPT_FILENAME']) ? '' : $_SERVER['SCRIPT_FILENAME']; $tmp2 = realpath(__FILE__); $i = strlen($tmp) - 1; $j = strlen($tmp2) - 1;
while ($i > 0 && $j > 0 && isset($tmp[$i]) && isset($tmp2[$j]) && $tmp[$i] == $tmp2[$j]) { $i--; $j--; }
if (!$res && $i > 0 && file_exists(substr($tmp, 0, ($i + 1))."/main.inc.php")) $res = @include substr($tmp, 0, ($i + 1))."/main.inc.php";
if (!$res && $i > 0 && file_exists(dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php")) $res = @include dirname(substr($tmp, 0, ($i + 1)))."/main.inc.php";
// Try main.inc.php using relative path
if (!$res && file_exists("../main.inc.php")) $res = @include "../main.inc.php";
if (!$res && file_exists("../../main.inc.php")) $res = @include "../../main.inc.php";
if (!$res && file_exists("../../../main.inc.php")) $res = @include "../../../main.inc.php";
if (!$res) die("Include of main fails");

require_once(DOL_DOCUMENT_ROOT."/core/lib/admin.lib.php");
require_once(DOL_DOCUMENT_ROOT."/core/lib/files.lib.php");
require_once(DOL_DOCUMENT_ROOT."/core/class/html.form.class.php");
require_once(DOL_DOCUMENT_ROOT."/core/class/html.formfile.class.php");

dol_include_once('tos/lib/tos.lib.php');

$langs->load("admin");
$langs->load("companies");
$langs->load("tos@tos");
$langs->load("other");
$langs->load("errors");

//TODO Check if module is ennabled
if (!$user->admin || empty($conf->tos->enabled))
{
   accessforbidden();
}

//Init error
$error = 0;
$message = false;

// TODO check get post arg
$action = GETPOST('action', 'alpha');
$tos = GETPOST('tos');
$urlfile = GETPOST('urlfile');
$confirm=GETPOST('confirm', 'alpha');

$upload_dir = $conf->tos->dir_output;

$html = new Form($db);
$formfile = new FormFile($db);

// Check if uploaded file is pdf
// TODO This is quick and dirty, think to update actions_linkedfiles.inc.php
//      to correctly support this functionality
if (GETPOST('sendit', 'alpha') && ! empty($conf->global->MAIN_UPLOAD_DOC))
{
	foreach($_FILES['userfile']['type'] as $type) {
		if ($type != 'application/pdf') {
			$message = $langs->trans('WrongFileExtension');
			$error++;
			break;
		}
	}
}
// Action to manage file
if (empty($error)) require_once DOL_DOCUMENT_ROOT.'/core/actions_linkedfiles.inc.php';

if ($action == 'toggletosoneachpage')
	dolibarr_set_const($db, 'ADD_TOS_ON_EACH_PAGE', ($conf->global->ADD_TOS_ON_EACH_PAGE ? '' : 1), 'chaine', 0, '', $conf->entity);

if ($action == 'updatedefaulttos') {
    $ret = dolibarr_set_const($db, 'TOS_DEFAULT_FILE', $tos, 'chaine', 0, '', $conf->entity);
	if ($ret) {
		$message = $langs->trans('CGVUpdated');
	} else {
		$message = $langs->trans('CGVUpdateFailded');
		$error++;
	}
}

// List of pdf file used for ToS
$upload_dir = $conf->tos->dir_output;
$files = dol_dir_list($upload_dir, 'files', 0, '\.pdf$', '\.meta$', '', SORT_ASC,1);
$files_for_select = array('NoCgv'=>$langs->trans('NoCgv'));
foreach ($files as $f)
	$files_for_select[$f['name']] = preg_replace('/\.[a-z]{3}$/', '', $f['name']);

// Update select extrafields for CGV
if (((GETPOST('sendit', 'alpha') && !empty($conf->global->MAIN_UPLOAD_DOC) && !empty($_FILES)) && (!empty($result) && $result > 0)) ||
	(($action == 'confirm_deletefile' && $confirm == 'yes') && $ret) ||
	($action == 'updatedefaulttos' && $ret)) {

	include_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
	$extrafields = new ExtraFields($db);
	foreach(array('commande', 'expedition', 'facture', 'propal') as $elem) {
		$res = $extrafields->update(
			$attrname = 'tos_attached',
			$label = 'CGV',
			$type = 'select',
			$length = 255,
			$elementtype = $elem,
			$unique = 0,
			$required = 1,
			$pos = 10,
			$param = array('options'=>$files_for_select),
			$alwayseditable = 0,
			$perms = '',
			$list = -1,
			$help = '',
			$default = (empty($conf->global->TOS_DEFAULT_FILE) ? 'NoCgv' : $conf->global->TOS_DEFAULT_FILE),
			$computed = '',
			$entity = '',
			$langfile = 'tos@tos',
			$enabled = '$conf->tos->enabled',
		);
		if ($res < 0) {
			$errdesc = __FILE__.' : Faild to update select list for ToS for '.$elem;
			dol_syslog($message, LOG_ERR);
			setEventMessage($messages, '', 'errors');
		}
	}
}

/*
 * View
 */

$imgAddToSPage = ($conf->global->ADD_TOS_ON_EACH_PAGE ?  img_picto($langs->trans("Activated"),'switch_on') : img_picto($langs->trans("Disabled"),'switch_off'));
$linkAddToSPage	= '<a href="'.$_SERVER['PHP_SELF'].'?action=toggletosoneachpage" >'.$imgAddToSPage.'</a>';

$head = tosAdminPrepareHead();

$current_head = 'config';

$linkback = '<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';

require_once("../tpl/admin.config.tpl.php");

$db->close();

?>
