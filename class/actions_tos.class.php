<?php
/* Copyright (C) 2012      Mikael Carlavan        <contact@mika-carl.fr>
 *                                                http://www.mikael-carlavan.fr
 * Copyright (C) 2020      Maxime DEMAREST        <maxime@indelog.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *      \file       htdocs/tos/class/tos.class.php
 *      \ingroup    tos
 *      \brief      File of class to add terms of sale
 */

require_once(DOL_DOCUMENT_ROOT ."/core/class/commonobject.class.php");
require_once(DOL_DOCUMENT_ROOT ."/core/lib/functions.lib.php");
require_once(DOL_DOCUMENT_ROOT ."/core/lib/functions2.lib.php");
require_once(DOL_DOCUMENT_ROOT.'/core/lib/price.lib.php');
require_once(DOL_DOCUMENT_ROOT."/core/lib/files.lib.php");
require_once DOL_DOCUMENT_ROOT.'/core/lib/pdf.lib.php';

/**
 *      \class      Tos
 *      \brief
 */
class ActionsToS
{

    /**
     * @var DoliDB Database handler.
     */
    public $db;

    /**
     * @var string Error code (or message)
     */
    public $error = '';

    /**
     * @var array Errors
     */
    public $errors = array();


    /**
     * @var array Hook results. Propagated to $hookmanager->resArray for later reuse
     */
    public $results = array();

    /**
     * @var string String displayed by executeHook() immediately after return
     */
    public $resprints;

    /**
     * Constructor
     *
     *  @param		DoliDB		$db      Database handler
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

	/**
	 * 	afterPDFCreation
	 * @param	array			$parameters		Array of parameters
	 * @param	CommonObject    $object         The object to process (an invoice if you are in invoice module, a propale in propale's module, etc...)
	 * @param	string			$action      	'add', 'update', 'view'
	 * @return	int         					<0 if KO,
	 *                           				=0 if OK but we want to process standard actions too,
	 *                            				>0 if OK and we want to replace standard actions.
	 */
	function afterPDFCreation($parameters=false, &$pdfclass, &$action='')
	{
		global $conf, $user, $langs, $db;

		include_once(DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php');

		$file = $parameters['file'];
		$object = $parameters['object'];
		$tosFilename = $object->array_options['options_tos_attached'];

		// Do nothing if no ToS
		if ((empty($tosFilename) || $tosFilename == 'NoCgv') ||
			($object->element != 'facture' && $object->element != 'commande' && $object->element != 'propal' && $objets->element != 'shipping'))
				return 0;

		$tosFilePath = $conf->tos->dir_output ."/". $tosFilename;

		if (!dol_is_file($tosFilePath) || dol_mimetype($tosFilePath) != 'application/pdf') {
			$errmsg = __METHOD__.' : Can\'t add '.$tosFilename.'. It isn\'t a pdf file.';
			dol_syslog($errmsg, LOG_ERR);
			$this->error = $errmsg;
			$this->errors[] = $errmsg;
			return -1;
		}

		$pdf = pdf_getInstance($pdfclass->format);
		$pdf->Open();
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// Load PDF file
		$pagesNbr = $pdf->setSourceFile($file);
		dol_syslog(__METHOD__.' : load file='.$file, LOG_DEBUG);

		for ($p = 1; $p <= $pagesNbr; $p++) {
			$templateIdx = $pdf->ImportPage($p);
			$size = $pdf->getTemplatesize($templateIdx);

			$portrait = $size['h'] > $size['w'] ? true : false;

			$pdf->AddPage($portrait ? 'P' : 'L');
			$pdf->useTemplate($templateIdx);

			if ($conf->global->ADD_TOS_ON_EACH_PAGE) {
				$pagesNbrTos = $pdf->setSourceFile($tosFilePath);
				dol_syslog(__METHOD__.' : load file='.$tosFilePath, LOG_DEBUG);
				for ($pTos = 1; $pTos <= $pagesNbrTos; $pTos++) {
					$templateIdx = $pdf->ImportPage($pTos);
					$size = $pdf->getTemplatesize($templateIdx);

					$pdf->AddPage(($size['h'] > $size['w']) ? 'P' : 'L');
					$pdf->useTemplate($templateIdx);
				}
				$pagesNbr = $pdf->setSourceFile($file);
			}
		}

		if (empty($conf->global->ADD_TOS_ON_EACH_PAGE)) {
			$pagesNbr = $pdf->setSourceFile($tosFilePath);
			dol_syslog(__METHOD__.' : load file='.$tosFilePath, LOG_DEBUG);
			for ($p = 1; $p <= $pagesNbr; $p++) {
				$templateIdx = $pdf->ImportPage($p);
				$size = $pdf->getTemplatesize($templateIdx);

				$pdf->AddPage(($size['h'] > $size['w']) ? 'P' : 'L');
				$pdf->useTemplate($templateIdx);
			}
		}

		// Save file
		if (method_exists($pdf,'AliasNbPages')) $pdf->AliasNbPages();

		$pdf->Close();

		$pdf->Output(DOL_DATA_ROOT.'/test.pdf','F');

		dol_syslog(__METHOD__.' : added Tos '.$tosFilename.' to file '.$file, LOG_DEBUG);
		$pdf->Output($file,'F');

		return 1;
	}

}


